variable "vpc_id" {
  type = string
}

variable "subnets_id" {
  type = list(string)
}

variable "alb_sg_name" {
  type = string
}

variable "ecs_task_sg_name" {
  type = string
}

variable "alb_name" {
  type = string
}

variable "alb_target_group_name" {
  type = string
}

variable "ecs_task_role_name" {
  type = string
}

variable "ecs_rds_policy_name" {
  type = string
}

variable "ecs_task_role_policy_attachment_name" {
  type = string
}

variable "ecs_task_execution_role_name" {
  type = string
}

variable "ecs_cluster_name" {
  type = string
}

variable "task_definition_name" {
  type = string
}

variable "container_name" {
  type = string
}

variable "container_image" {
  type = string
}

variable "container_vcpu" {
  type = number
}

variable "container_memory" {
  type = number
}

variable "container_port" {
  type = number
}

variable "container_env_vars" {
  type = list
}

variable "container_count" {
  type = number
}

variable "ecs_service_name" {
  type = string
}
