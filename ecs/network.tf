resource "aws_security_group" "infinant_alb_sg" {
  name   = var.alb_sg_name
  vpc_id = var.vpc_id

  tags = {
    Name = var.alb_sg_name
  }
}

resource "aws_security_group_rule" "http_ingress_rule" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.infinant_alb_sg.id
}

resource "aws_security_group_rule" "https_ingress_rule" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.infinant_alb_sg.id
}

resource "aws_security_group_rule" "alb_egress_rule" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.infinant_alb_sg.id
}

resource "aws_security_group" "infinant_ecs_task_sg" {
  name   = var.ecs_task_sg_name
  vpc_id = var.vpc_id

  tags = {
    Name = var.ecs_task_sg_name
  }
}

resource "aws_security_group_rule" "container_ingress_rule" {
  type              = "ingress"
  from_port         = var.container_port
  to_port           = var.container_port
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.infinant_ecs_task_sg.id
}

resource "aws_security_group_rule" "ecs_task_egress_rule" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.infinant_ecs_task_sg.id
}

resource "aws_lb" "infinant_ecs_alb" {
  name                       = var.alb_name
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.infinant_alb_sg.id]
  subnets                    = var.subnets_id
  enable_deletion_protection = false
}

resource "aws_alb_target_group" "infinant_alb_target_group" {
  name        = var.alb_target_group_name
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = "5"
    interval            = "60"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/health-check"
    unhealthy_threshold = "2"
  }
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = aws_lb.infinant_ecs_alb.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = 8080
      protocol    = "HTTP"
      status_code = "HTTP_301"
    }
  }
}

