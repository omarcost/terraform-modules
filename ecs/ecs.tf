resource "aws_ecs_cluster" "infinant_ecs_cluster" {
  name = var.ecs_cluster_name
}

resource "aws_ecs_task_definition" "infinant_ecs_task_definition" {
  family                   = var.task_definition_name
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.container_vcpu
  memory                   = var.container_memory
  execution_role_arn       = aws_iam_role.infinant_ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.infinant_ecs_task_role.arn

  container_definitions    = jsonencode([{
    name        = var.container_name
    image       = var.container_image
    essential   = true
    environment = var.container_env_vars
    portMapping = [{
      protocol      = "tcp"
      containerPort = var.container_port
      hostPort      = var.container_port
    }]
  }])
}

resource "aws_ecs_service" "infinant_ecs_service" {
  name                               = var.ecs_service_name
  cluster                            = aws_ecs_cluster.infinant_ecs_cluster.id
  task_definition                    = aws_ecs_task_definition.infinant_ecs_task_definition.arn
  desired_count                      = var.container_count
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 100
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    subnets          = var.subnets_id
    security_groups  = [aws_security_group.infinant_ecs_task_sg.id]
    assign_public_ip = false
  }

  load_balancer {
    container_name   = var.container_name
    container_port   = var.container_port
    target_group_arn = aws_alb_target_group.infinant_alb_target_group.arn
  }

  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }
}

resource "aws_appautoscaling_target" "ecs_container_autoscaling" {
  max_capacity       = 2
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.infinant_ecs_cluster.name}/${aws_ecs_service.infinant_ecs_service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_autoscaling_policy" {
  name               = "${var.container_name}-ausocaling-policy"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_container_autoscaling.id
  scalable_dimension = aws_appautoscaling_target.ecs_container_autoscaling.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_container_autoscaling.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value = 60
  }
}