data "aws_iam_policy_document" infinant_ecs_task_policy {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "infinant_ecs_task_role" {
  name               = var.ecs_task_role_name
  assume_role_policy = data.aws_iam_policy_document.infinant_ecs_task_policy.json
}

data "aws_iam_policy_document" "infinant_rds_policy_document" {
  statement {
    actions = ["rds:*"]
    effect  = "Allow"
    resources = ["*"]
  }
}

resource "aws_iam_policy" "infinant_rds_policy" {
  name   = var.ecs_rds_policy_name
  policy = data.aws_iam_policy_document.infinant_rds_policy_document.json
}

resource "aws_iam_policy_attachment" "infinant_ecs_task_role_policy_attachment" {
  name       = var.ecs_task_role_policy_attachment_name
  roles      = [aws_iam_role.infinant_ecs_task_role.name]
  policy_arn = aws_iam_policy.infinant_rds_policy.arn
}

data "aws_iam_policy_document" "infinant_ecs_task_execution_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "infinant_ecs_task_execution_role" {
  name               = var.ecs_task_execution_role_name
  assume_role_policy = data.aws_iam_policy_document.infinant_ecs_task_execution_policy.json
}

resource "aws_iam_role_policy_attachment" "infinant_ecs_task_execution_role_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.infinant_ecs_task_execution_role.name
}