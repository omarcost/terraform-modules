data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_db_subnet_group" "infinant_rds_subnet_group" {
  name       = var.subnet_group_name
  subnet_ids = var.subnets_id

  tags = {
    Name = var.subnet_group_name
  }
}

resource "aws_rds_cluster" "infinant_rds_cluster" {
  count              = var.subnet_quantity
  cluster_identifier = var.rds_cluster_name
  engine             = "aurora-mysql"
  engine_version     = "5.7.mysql_aurora.2.03.2"
  availability_zones = data.aws_availability_zones.available.names[count.index]
  database_name      = "person"
  master_username    = "admin"
  master_password    = "admin"
}

resource "aws_rds_cluster_instance" "infinant_rds_cluster_instance" {
  cluster_identifier = aws_rds_cluster.infinant_rds_cluster.id
  instance_class     = "db.t3.small"
  engine             = aws_rds_cluster.infinant_rds_cluster.engine
  engine_version     = aws_rds_cluster.infinant_rds_cluster.engine_version
}