variable "subnets_id" {
  type = list(string)
}

variable "subnet_group_name" {
  type = string
}

variable "subnet_quantity" {
  default = 1
  type = string
  description = "Quantity of subnets to create in VPC"
}

variable "rds_cluster_name" {
  type = string
}
